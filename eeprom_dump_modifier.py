 
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Author:                     Hendrik Schutter, mail@hendrikschutter.com
    Date of creation:           2021/10/16
    Date of last modification:  2021/10/16

"""
import hashlib

old_entry_secure_boot_str = "AA553F0003000000000000000000000000000000000000000000000000000000000000002200000001000000C70BA3F008AF564599C4001009C93A4453006500630075007200650042006F006F00740045006E00610062006C006500000001"
new_entry_secure_boot_str = "AA55000003000000000000000000000000000000000000000000000000000000000000002200000001000000C70BA3F008AF564599C4001009C93A4453006500630075007200650042006F006F00740045006E00610062006C006500000001"

old_eeprom_dump = "with_SecBoot.bin"
new_eeprom_dump = "with_SecBoot_modified.bin"

#old_eeprom_dump = "test.bin"
#new_eeprom_dump = "test_modified.bin"

old_entry_secure_boot_bytes = [old_entry_secure_boot_str.lower()[i:i+2] for i in range(0, len(old_entry_secure_boot_str.lower()), 2)]
new_entry_secure_boot_bytes = [new_entry_secure_boot_str.lower()[i:i+2] for i in range(0, len(new_entry_secure_boot_str.lower()), 2)]

def show_file_stats():
    print(" === old eeprom dump ===")
    tmp_file = open(old_eeprom_dump, 'rb')
    hash = str(hashlib.sha256(tmp_file.read()).hexdigest())
    tmp_file.seek(0,2)
    print("Size: " + str(tmp_file.tell()))
    print("Hash: " + hash)
    tmp_file.close()

    try:
        tmp_file = open(new_eeprom_dump, 'rb')
        hash = str(hashlib.sha256(tmp_file.read()).hexdigest())
        print("\n ===new eeprom dump === ")
        tmp_file.seek(0,2)
        print("Size: " + str(tmp_file.tell()))
        print("Hash: " + hash)
    except IOError:
        pass
    finally:
        tmp_file.close()

def get_pattern_offset(data):
    pattern_start_index = -1
    byte_index = 0
    for byte in data:
        if(byte.hex() == old_entry_secure_boot_bytes[0]):
            if pattern_start_index == -1:
                pattern_start_index = byte_index
                for x in range(pattern_start_index, pattern_start_index+len(old_entry_secure_boot_bytes)): 
                    if(data[x].hex() != old_entry_secure_boot_bytes[(x-pattern_start_index)]):
                        pattern_start_index = -1
                        break
        byte_index += 1
    return pattern_start_index


def replace_data(data, replacement, offset):
    for x in range(offset, offset+len(replacement)): 
        data[x] = bytes.fromhex(replacement[x-offset])

def edit_data(data, new_eeprom_dump_file):
    offset = get_pattern_offset(data)
    
    if offset != -1:
        print("Pattern offset: " + str(offset)) 
        
        replace_data(data, new_entry_secure_boot_bytes, offset)
        
        for byte in data:
            #print(byte.hex())
            new_eeprom_dump_file.write(byte)
        new_eeprom_dump_file.flush()
    else:
        print("unable to find pattern")
    
def main():
    print("start ...\n")
    
    if(len(old_entry_secure_boot_str) != len(new_entry_secure_boot_str)):
        print("entries are not the same sizes")
        return
    
    old_eeprom_dump_file = open(old_eeprom_dump, "rb")
    new_eeprom_dump_file = open(new_eeprom_dump, "wb")
    
    old_eeprom_dump_data = []
    
    byte = old_eeprom_dump_file.read(1)
    while byte:
        old_eeprom_dump_data.append(byte)
        byte = old_eeprom_dump_file.read(1)
        
    edit_data(old_eeprom_dump_data, new_eeprom_dump_file)
        
    #time.sleep(100)
    show_file_stats()
    
if __name__ == "__main__":
    main()










